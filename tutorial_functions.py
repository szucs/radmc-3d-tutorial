import matplotlib.pyplot as plt
import numpy as np
import astropy.constants as c
import astropy.units as u
import radmc3dPy


def plotmodels(data,ddens=True,dtemp=True,Tmin=0.,Tmax=50.,xlog=True,
		vmin=1e-30):
   '''
   Takes a radm3dPy radmc3dData object and plots the dust density and/or 
   dust temperature distribution within the model.
   '''
   # Units
   au = u.au.to(u.centimeter)
   # Plot the density
   ngs = data.rhodust.shape[3]
   if ddens:
      f, ax = plt.subplots(ngs,1, figsize=(10,8*ngs))
      if ngs == 1:
        ax= [ax]
      for i in range(ngs):
        plt.subplot(ax[i])
        rhodust = data.rhodust[:,:,0,i]
        rhodust[rhodust<1e-99] = np.nan
        PltDens = plt.contourf(data.grid.x/au, np.pi/2.-data.grid.y, 
                                np.log10(rhodust.T), 60)
        plt.xlabel('r [AU]')
        plt.ylabel(r'$\pi/2-\theta$')
        if xlog:
            plt.xscale('log')
        cbD = plt.colorbar(PltDens)
        cbD.set_label(r'$\log_{10}{\rho}$')
      plt.show()

   # Plot the temperature
   if dtemp:
      f, ax = plt.subplots(ngs,1, figsize=(10,8*ngs))
      if ngs == 1:
        ax= [ax]
      for i in range(ngs):
        plt.subplot(ax[i])
        Tdust = data.dusttemp[:,:,0,i].T.clip(Tmin,Tmax)
        Tdust[Tdust<1e-99] = np.nan
        PltTemp = plt.contourf(data.grid.x/au, np.pi/2.-data.grid.y,
				Tdust, 60, vmin=vmin)
        plt.xlabel('r [AU]')
        plt.ylabel(r'$\pi/2-\theta$')
        if xlog:
            plt.xscale('log')
        cbT = plt.colorbar(PltTemp)
        cbT.set_label('T [K]') 
      plt.show()



def plot_obs_sed(phot, irs, legend=False):
    '''
    Plot photometric, submm and mid-IR spectroscopic data on a common figure.
    The astropy Tables taken as arguments are prepared using the Create-obs-SED.ipynb
    notebook.
    '''
    phot_flux = phot['flux'] * u.Jansky
    phot_eflux = phot['eflux'] * u.Jansky
    phot_freq = phot['freq'] * u.Hz
    
    irs_flux = irs['flux'] * u.Jansky
    irs_eflux = irs['eflux'] * u.Jansky
    irs_freq = irs['freq'] * u.Hz
    
    plt.errorbar(phot['wavelength'], (phot_flux*phot_freq).to(u.erg/u.s/u.cm/u.cm).value, 
                 yerr=(phot_eflux*phot_freq).to(u.erg/u.s/u.cm/u.cm).value, 
                 fmt='o', label='Photometry')

    plt.fill_between(irs['wavelength'], ( (irs_flux - irs_eflux) * irs_freq ).to(u.erg/u.s/u.cm/u.cm).value, 
                 ( (irs_flux + irs_eflux) * irs_freq ).to(u.erg/u.s/u.cm/u.cm).value, 
                 facecolor='gray', alpha=1.0, edgecolor='none')

    plt.plot(irs['wavelength'], ( irs_flux * irs_freq ).to(u.erg/u.s/u.cm/u.cm).value, 
             label='IRS')
    
    plt.title('SED of DM Tau')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Wavelength ($\mu$m)')
    plt.ylabel('Flux density (erg cm$^{-2}$ s$^{-1}$)')
    if legend:
    	plt.legend()
    plt.grid()

def plot_model_sed(fname='spectrum.out', dpc=150.,oplot=False):
    '''
    Plots the SED or spectrum in spectrum.out and scales the 
    flux with the object distance, dpc, in parsecs.
    '''
    sed = radmc3dPy.analyze.readSpectrum(fname=fname)
    wavelength = sed[:,0]                             # in microns
    freq = c.c / (wavelength * u.micrometer).to(u.m)
    SED = sed[:,1]                                    # Flux density (erg/s/cm^2/Hz) @ 1 pc!

    plt.loglog(wavelength,SED/dpc**2 * freq)
    plt.xlabel('Wavelength ($\mu$m)')
    plt.ylabel('$\\nu$ F$_{\\nu}$ (erg cm$^{-2}$ s$^{-1}$)')

